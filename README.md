# C++ Primer 4th Edition Solutions
These are my solutions to the questions in the book C++ Primer 4th Edition. I know it's an old book but I just wanted to learn the basics and other editions weren't available in my region. If there's any mistake, please submit a merge request!
